<?php


namespace App\Api\Controllers\V1\User;


use App\Api\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function register(Request $request){
        User::create([
            'username'=>$request->username,
            'password'=>Hash::make($request->password)
        ]);
        return $this->success('0',[],'创建成功');
    }

    public function test(Request $request){

        return $this->success('0',[],'创建成功');
    }
}
