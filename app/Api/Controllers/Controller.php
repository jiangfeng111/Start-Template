<?php


namespace App\Api\Controllers;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($code,$data,$message){

        return response()->json(['code'=>$code,'data'=>$data,'message'=>$message]);
    }

    public function error($code,$data,$message){

        return response()->json(['code'=>$code,'data'=>$data,'message'=>$message]);
    }

    public function user()
    {
        return response()->json(auth('api')->user());
    }
}
