<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//用户验证
Route::group([
    'prefix' => 'auth',
    'middleware'=>'api.auth',
    ], function () {
    Route::post('logout', [\App\Api\Controllers\V1\User\AuthController::class,'logout']);
    Route::post('me', [\App\Api\Controllers\V1\User\AuthController::class,'me']);
});

Route::group(['prefix' => 'auth',], function () {
    Route::post('refresh', [\App\Api\Controllers\V1\User\AuthController::class,'refresh']);
    Route::post('register', [\App\Api\Controllers\V1\User\UserController::class,'register']);
    Route::post('login', [\App\Api\Controllers\V1\User\AuthController::class,'login']);
});

//其他接口
Route::group([
    'prefix' => 'user',
    'middleware'=>'api.auth',
], function () {
    Route::post('test', [\App\Api\Controllers\V1\User\UserController::class,'test']);
});
